# --------------------------------------------------------
#
# Structure of the table 'edara_user'
#
CREATE TABLE edara_user (
   user_id smallint(5) unsigned NOT NULL auto_increment,
   user_register_date int(11) DEFAULT '0' NOT NULL,
   user_name tinytext NOT NULL,
   user_email tinytext NOT NULL,
   user_passwd tinytext NOT NULL,
   user_bday tinytext NOT NULL,
   user_from tinytext NOT NULL,
   user_at tinytext NOT NULL,
   user_skill tinytext NOT NULL,
   user_desc text NOT NULL,
   user_priv tinyint(4) DEFAULT '0' NOT NULL,
   user_options tinytext NOT NULL,
   user_login timestamp(14),
   PRIMARY KEY (user_id)
);


# --------------------------------------------------------
#
# Structure of the table 'edara_calendar'
#

CREATE TABLE edara_calendar (
   cal_id smallint(5) unsigned NOT NULL auto_increment,
   cal_user_id smallint(5) unsigned DEFAULT '0' NOT NULL,
   cal_approved char(1) NOT NULL,
   cal_create_date int(11) DEFAULT '0' NOT NULL,
   cal_year int(4) unsigned DEFAULT '0' NOT NULL,
   cal_month int(2) unsigned DEFAULT '0' NOT NULL,
   cal_day_num int(2) unsigned DEFAULT '0' NOT NULL,
   cal_day_alph char(3) NOT NULL,
   cal_subject tinytext NOT NULL,
   cal_desc text NOT NULL,
   cal_private char(1) DEFAULT 'N' NOT NULL,
   cal_duration smallint(6) DEFAULT '0' NOT NULL,
   cal_task_valid char(1) DEFAULT 'N' NOT NULL,
   cal_task_id smallint(5) unsigned DEFAULT '0' NOT NULL,
   cal_repeated varchar(4) DEFAULT 'NO' NOT NULL,
   cal_rep_by_num char(1) NOT NULL,
   cal_rep_days varchar(7) NOT NULL,
   cal_rep_freq int(11) DEFAULT '0' NOT NULL,
   cal_rep_end int(11) DEFAULT '0' NOT NULL,
   cal_email text NOT NULL,
   PRIMARY KEY (cal_id)
);


# --------------------------------------------------------
#
# Structure of the table 'edara_events'
#

CREATE TABLE edara_events (
   event_id smallint(5) NOT NULL auto_increment,
   event_title varchar(255) NOT NULL,
   event_start_date datetime,
   event_end_date datetime,
   event_parent smallint(5) unsigned DEFAULT '0' NOT NULL,
   event_description text,
   event_times_recuring int(11) unsigned DEFAULT '0' NOT NULL,
   event_recurs int(11) unsigned DEFAULT '0' NOT NULL,
   event_remind int(10) unsigned DEFAULT '0' NOT NULL,
   PRIMARY KEY (event_id),
   KEY id_esd (event_start_date),
   KEY id_eed (event_end_date),
   KEY id_evp (event_parent)
);


# --------------------------------------------------------
#
# Structure of the table 'edara_projects'
#

CREATE TABLE edara_projects (
   proj_id smallint(5) NOT NULL auto_increment,
   proj_name varchar(255) NOT NULL,
   proj_desc text,
   proj_admin smallint(5),
   proj_url varchar(255),
   proj_start_date datetime,
   proj_end_date datetime,
   proj_actual_date datetime,
   proj_status int(11),
   proj_perc_complete tinyint(4),
   proj_color varchar(6),
   proj_users text NOT NULL,
   PRIMARY KEY (proj_id)
);


# --------------------------------------------------------
#
# Structure of the table 'edara_task'
#

CREATE TABLE edara_task (
   task_id smallint(5) unsigned NOT NULL auto_increment,
   task_owner smallint(5) DEFAULT '0' NOT NULL,
   task_proj smallint(5) DEFAULT '0' NOT NULL,
   task_from_id smallint(5) unsigned DEFAULT '0' NOT NULL,
   task_to_id smallint(5) unsigned DEFAULT '0' NOT NULL,
   task_approved char(1) NOT NULL,
   task_subject tinytext NOT NULL,
   task_create_date int(11) DEFAULT '0' NOT NULL,
   task_due_date tinyint(4) DEFAULT '0' NOT NULL,
   task_status tinytext NOT NULL,
   task_desc text NOT NULL,
   task_email text NOT NULL,
   PRIMARY KEY (task_id)
);


# --------------------------------------------------------
#
# Structure of the table 'edara_voln_tasks'
#

CREATE TABLE edara_user_tasks (
   user_id smallint(5) DEFAULT '0' NOT NULL,
   user_type tinyint(4) DEFAULT '0' NOT NULL,
   task_id smallint(5) DEFAULT '0' NOT NULL,
   PRIMARY KEY (user_id, task_id),
   KEY user_type (user_type)
);

