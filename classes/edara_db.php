<?php
//Start Configurations
//server name
define("DB_SERVER", "localhost");
//username and password for the database
define("DB_USERNAME", "Your mySQL username here");
define("DB_PASSWORD", "Shh! don't tell anyone");
//the name of the database you intend to use for eDara
define("DB_NAME", "edara");
//End Configurations


//--------------------------------------------------------------------------
//a class for mySQL connections (a mySQL connection is the object)
class mysql_db_link {
  //Data Members
	var $db_server;
	var $db_name;
	var $db_username;
	var $db_password;
	var $query_st;
    var $result;
    var $fetched;
    var $num_rows;
    var $prev_id;
	var $link;

  //Methods
	//open a connection
    function connect() {
        $this->link = mysql_connect($this->db_server, $this->db_username, $this->db_password)
        or die ("Error connecting to the database!");
        if(!mysql_select_db($this->db_name, $this->link)) { //the database specified cannot be used!
			$this->display_error("the database ".$this->db_name." cannot be used for eDara or it doesn't exist!");
		}
    }
	//close that connection
    function close() {
        if (!mysql_close($this->link)) {
				$this->display_error("Cannot close the database ".$this->db_name."!");
        }
    }
	//do a query $query_st to the connection in $link
    function query() {
		$this->$result = mysql_query($this->query_st)
        or die ("Error in this query!");
    }
	//fetch the resulting array $result into entries in $fetched
    function fetch() {
		$this->fetched = mysql_fetch_array($this->result);
    }
	//get the id generated from the previous INSERT query
	function insert_id() {
		$this->prev_id = mysql_insert_id($this->link);
	}
	//report errors , (if any;))
    function display_error($an_error) {
		//some code that will (somehow;) display the string variable $an_error
		//I'll do that later :)
    }
	//returns the count of rows in current resultset
	function numrows() {
		$this->num_rows = @mysql_num_rows($this->result);
	}

	//frees the result set
	function freeResult() {
		@mysql_free_result($this->result);
	}

	
}
//-------------------------------------------------------------------------------


//initialise an object for using by different modules
$mydb=new mysql_db_link();
$mydb->db_server=DB_SERVER;
$mydb->db_username=DB_USERNAME;
$mydb->db_password=DB_PASSWORD;
$mydb->db_name=DB_NAME;




//*********************************************************************************
//Documentation
//*********************************************************************************


//------------------------
//how to use this class ?
//------------------------
//here is a sample using this class with the '$mydb' object:
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	$mydb->connect();
//  $mydb->query("SELECT * FROM my_table")
//
//	print "<table>\n";
//	while($line = $mydb->fetch()){
//		print "\t<tr>\n";
//		while(list($col_name, $col_value) = each($line)){
//			print "\t\t<td>$col_value</td>\n";
//		}
//		print "\t</tr>\n";
//	}
//	print "</table>\n";
//
//       $mydb->freeResult();
//       $mydb->close();
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//-------------------------------
//the advantages of using this class
//-------------------------------
//- not to worry about errors
//- it makes the life easier for you
//- it makes you concentrate on what you are doing and not to care about how to do it
//- it will be very easy if we need to change it later rather than changing all the modules
//- we can do another one to deal with a different type of databases (Oracle, postgreSQL..etc)

