<?php
require(./edara_db.php");

//--------------------------------------------------------------------------
//a class for projects connections 
class project {
  //Data Members
  var $id;
  var $name;
  var $desc;
  var $admin;
  var $url;
  var $start_date;
  var $end_date;
  var $actual_date;
  var $status;
  var $perc;
  var $color;
  var $users;
  

  //Constructor
  function project($id=null) {
	$this->id=$id;
	$this->name=null;
	$this->desc=null;
	$this->admin=null;
	$this->url=null;
	$this->start_date=null;
	$this->end_date=null;
	$this->actual_date=null;
	$this->status=null;
	$this->perc=null;
	$this->color=null;
	$this->users=null;
	if ($id) {
		$mydb->connect();
		$mydb->query("SELECT * FROM edara_projects WHERE proj_id=$id");
		if ( $mydb->numrows() ) {
			$mydb->fetch();
			$this->name=$mydb->fetched["proj_name"];
			$this->desc=$mydb->fetched["proj_desc"];
			$this->admin=$mydb->fetched["proj_admin"];
			$this->url=$mydb->fetched["proj_url"];
			$this->start_date=$mydb->fetched["proj_start_date"];
			$this->end_date=$mydb->fetched["proj_end_date"];
			$this->actual_date=$mydb->fetched["proj_actual_date"];
			$this->status=$mydb->fetched["proj_status"];
			$this->perc=$mydb->fetched["proj_perc_complete"];
			$this->color=$mydb->fetched["proj_color"];
			$this->users=explode("SEP" , $mydb->fetched["proj_users"] );
			$mydb->freeResult();
		}
		$mydb->close();
	}

  }
	
  //set,get functions
  function get_id() {
	return $this->id;
  }
 
  function set_name($nam) {
	$this->name=$nam;
  }
  function get_name() {
	return $this->name;
  }

  function set_desc($des) {
	$this->desc=$des;
  }
  function get_desc() {
	return $this->desc;
  }

  function set_admin($adm) {
	$this->desc=$adm;
  }
  function get_admin() {
	return $this->admin;
  }

  function set_url($ur) {
	$this->url=$ur;
  }
  function get_url() {
	return $this->url;
  }

  function set_start_date($sdate) {
	$this->start_date=$sdate;
  }
  function get_start_date() {
	return $this->start_date;
  }

  function set_end_date($edate) {
	$this->end_date=$edate;
  }
  function get_end_date() {
	return $this->end_date;
  }

  function set_actual_date($adate) {
	$this->actual_date=$adate;
  }
  function get_actual_date() {
	return $this->actual_date;
  }
	
  function set_status($stat) {
	$this->status=$stat;
  }
  function get_status() {
	return $this->status;
  }
	
  function set_perc($per) {
	$this->perc=$per;
  }
  function get_perc() {
	return $this->perc;
  }

  function set_color(colour) {
	$this->color=$colour;
  }
  function get_color() {
	return $this->color;
  }

  function get_users() {
	return $this->users;
  }


  function add_user($user) {
	array_push( $this->users , $user );	
  }


  //Add a new project into the database
  function store() {

  }

  //update an existing project with the current values
  function update() {

  }



}
?>
