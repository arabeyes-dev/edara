<?php

//Q:Which file will call the register_user function ?
//A:The main file index.php will include  link for non-logged in users to register,
//  the relative link should be something like that :
//      modules/users/edara_users.php?action=register
//
//Q:Which file will call the main function ?
//A:The main file index.php will include a login form that calls the main function
//  in this module using POST method and passing the user'email and user's password.




require('../../classes/edara_db.php');
require('../../classes/edara_users.php');


//Decide what to display
if       ($HTTP_GET_VARS['action'] == 'register_user' ) {
    $usr=new user();
    register_user();
} elseif ($HTTP_POST_VARS['action'] == 'do_register_user' ) {
    $usr=new user();
    $usr->user_init();
    $usr->us_name=$HTTP_POST_VARS['us_name_f'];
    $usr->us_email=$HTTP_POST_VARS['us_email_f'];
    $usr->us_passwd=$HTTP_POST_VARS['us_passwd_f'];
    $usr->us_bday=$HTTP_POST_VARS['byear'].$HTTP_POST_VARS['bmonth'].$HTTP_POST_VARS['bday'];
    $usr->us_from=$HTTP_POST_VARS['us_from_f'];
    $usr->us_at=$HTTP_POST_VARS['us_at_f'];
    $usr->us_skill=$HTTP_POST_VARS['us_skill_f'];
    $usr->us_desc=$HTTP_POST_VARS['us_desc_f']);
    $usr->us_options=$HTTP_POST_VARS['us_options_f']);
    do_register_user();

} elseif ($HTTP_POST_VARS['action'] == 'approve_user' ) {
    approve_user();
} elseif ($HTTP_POST_VARS['action'] == 'do_approve_user' ) {
    do_approve_user();
} elseif ($HTTP_POST_VARS['action'] == 'query_user' ) {
    query_user();
} elseif ($HTTP_POST_VARS['action'] == 'do_query_user' ) {
    do_query_user();
} elseif ($HTTP_POST_VARS['action'] == 'remove_user' ) {
    remove_user();
} elseif ($HTTP_POST_VARS['action'] == 'do_remove_user' ) {
    do_remove_user();
} else {
    $usr=new user();
    $usr->user_init();
    $usr->us_email=$HTTP_POST_VARS['us_email_f'];
    $usr->us_passwd=$HTTP_POST_VARS['us_passwd_f'];
    main();
}

//------------------------------------------------------------------------------
//this function is the main user interface for this module
function main() {
    global $PHP_SELF;
    global $usr;
         //call a function that will return the current user priv (using cookies)
         //A privillege of 0 means that no user exists with that email/password
         $userpriv=$usr->getpriv();
         if ($userpriv == 0 ) {
            //the user is not registered or not logged in and thus aren't allowed to use this module
            //return him to the main page index.php to either login or register
            //The login form will be in index.php and the registeration form will be in this module
            <?php
         	exit();
         } else if ($userpriv == 2 ) {
            //the user is a regular user
            //he can only query users
            ?>
            <a href="<?php echo $PHP_SELF; ?>?action=query_user">Query an user</a><br>
            <?php
            exit();
         } else if ($userpriv == 1 ) {
            //the user is an admin
            //he can can do everything ;)
            ?>
            <a href="<?php echo $PHP_SELF; ?>?action=query_user">Query an User</a><br>
            <a href="<?php echo $PHP_SELF; ?>?action=approve_user">Approve an User</a><br>
            <a href="<?php echo $PHP_SELF; ?>?action=invite_user">Invite an User</a><br>
            <a href="<?php echo $PHP_SELF; ?>?action=remove_user">Remove an User</a><br>
            <?php
         }
}
//end of the main function
//------------------------------------------------------------------------------





//------------------------------------------------------------------------------
//this functions sends a message to malicious users who try to make things they aren't allowed to
function go_away_message() {
         echo "Go play away kid, you can't destroy our system.";
         exit();
}
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//this functions prints option elements for all countries
function show_countries() {
    ?>
    <option value="Afghanistan">Afghanistan
    <option value="Albania">Albania
    <option value="Algeria">Algeria
    <option value="American Samoa">American Samoa
    <option value="Andorra">Andorra
    <option value="Angola">Angola
    <option value="Anguilla">Anguilla
    <option value="Antarctica">Antarctica
    <option value="Antigua And Barbuda">Antigua And Barbuda
    <option value="Argentina">Argentina
    <option value="Armenia">Armenia
    <option value="Aruba">Aruba
    <option value="Australia">Australia
    <option value="Austria">Austria
    <option value="Azerbaijan">Azerbaijan
    <option value="Bahamas, The">Bahamas, The
    <option value="Bahrain">Bahrain
    <option value="Bangladesh">Bangladesh
    <option value="Barbados">Barbados
    <option value="Belarus">Belarus
    <option value="Belgium">Belgium
    <option value="Belize">Belize
    <option value="Benin">Benin
    <option value="Bermuda">Bermuda
    <option value="Bhutan">Bhutan
    <option value="Bolivia">Bolivia
    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina
    <option value="Botswana">Botswana
    <option value="Bouvet Island">Bouvet Island
    <option value="Brazil">Brazil
    <option value="British Indian Ocean Territory">British Indian Ocean Territory
    <option value="Brunei">Brunei
    <option value="Bulgaria">Bulgaria
    <option value="Burkina Faso">Burkina Faso
    <option value="Burundi">Burundi
    <option value="Cambodia">Cambodia
    <option value="Cameroon">Cameroon
    <option value="Canada">Canada
    <option value="Cape Verde">Cape Verde
    <option value="Cayman Islands">Cayman Islands
    <option value="Central African Republic">Central African Republic
    <option value="Chad">Chad
    <option value="Chile">Chile
    <option value="China">China
    <option value="Christmas Island">Christmas Island
    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands
    <option value="Colombia">Colombia
    <option value="Comoros">Comoros
    <option value="Congo">Congo
    <option value="Congo, Democractic Republic of the">Congo, Democractic Republic of the
    <option value="Cook Islands">Cook Islands
    <option value="Costa Rica">Costa Rica
    <option value="Cote D'Ivoire (Ivory Coast)">Cote D'Ivoire (Ivory Coast)
    <option value="Croatia (Hrvatska)">Croatia (Hrvatska)
    <option value="Cuba">Cuba
    <option value="Cyprus">Cyprus
    <option value="Czech Republic">Czech Republic
    <option value="Denmark">Denmark
    <option value="Djibouti">Djibouti
    <option value="Dominica">Dominica
    <option value="Dominican Republic">Dominican Republic
    <option value="East Timor">East Timor
    <option value="Ecuador">Ecuador
    <option value="Egypt">Egypt
    <option value="El Salvador">El Salvador
    <option value="Equatorial Guinea">Equatorial Guinea
    <option value="Eritrea">Eritrea
    <option value="Estonia">Estonia
    <option value="Ethiopia">Ethiopia
    <option value="Falkland Islands (Islas Malvinas)">Falkland Islands (Islas Malvinas)
    <option value="Faroe Islands">Faroe Islands
    <option value="Fiji Islands">Fiji Islands
    <option value="Finland">Finland
    <option value="France">France
    <option value="French Guiana">French Guiana
    <option value="French Polynesia">French Polynesia
    <option value="French Southern Territories">French Southern Territories
    <option value="Gabon">Gabon
    <option value="Gambia, The">Gambia, The
    <option value="Georgia">Georgia
    <option value="Germany">Germany
    <option value="Ghana">Ghana
    <option value="Gibraltar">Gibraltar
    <option value="Greece">Greece
    <option value="Greenland">Greenland
    <option value="Grenada">Grenada
    <option value="Guadeloupe">Guadeloupe
    <option value="Guam">Guam
    <option value="Guatemala">Guatemala
    <option value="Guinea">Guinea
    <option value="Guinea-Bissau">Guinea-Bissau
    <option value="Guyana">Guyana
    <option value="Haiti">Haiti
    <option value="Heard and McDonald Islands">Heard and McDonald Islands
    <option value="Honduras">Honduras
    <option value="Hong Kong S.A.R.">Hong Kong S.A.R.
    <option value="Hungary">Hungary
    <option value="Iceland">Iceland
    <option value="India">India
    <option value="Indonesia">Indonesia
    <option value="Iran">Iran
    <option value="Iraq">Iraq
    <option value="Ireland">Ireland
    <option value="Italy">Italy
    <option value="Jamaica">Jamaica
    <option value="Japan">Japan
    <option value="Jordan">Jordan
    <option value="Kazakhstan">Kazakhstan
    <option value="Kenya">Kenya
    <option value="Kiribati">Kiribati
    <option value="Korea">Korea
    <option value="Korea, North">Korea, North
    <option value="Kuwait">Kuwait
    <option value="Kyrgyzstan">Kyrgyzstan
    <option value="Laos">Laos
    <option value="Latvia">Latvia
    <option value="Lebanon">Lebanon
    <option value="Lesotho">Lesotho
    <option value="Liberia">Liberia
    <option value="Libya">Libya
    <option value="Liechtenstein">Liechtenstein
    <option value="Lithuania">Lithuania
    <option value="Luxembourg">Luxembourg
    <option value="Macau S.A.R.">Macau S.A.R.
    <option value="Macedonia, Former Yugoslav Republic of">Macedonia, Former Yugoslav Republic of
    <option value="Madagascar">Madagascar
    <option value="Malawi">Malawi
    <option value="Malaysia">Malaysia
    <option value="Maldives">Maldives
    <option value="Mali">Mali
    <option value="Malta">Malta
    <option value="Marshall Islands">Marshall Islands
    <option value="Martinique">Martinique
    <option value="Mauritania">Mauritania
    <option value="Mauritius">Mauritius
    <option value="Mayotte">Mayotte
    <option value="Mexico">Mexico
    <option value="Micronesia">Micronesia
    <option value="Moldova">Moldova
    <option value="Monaco">Monaco
    <option value="Mongolia">Mongolia
    <option value="Montserrat">Montserrat
    <option value="Morocco">Morocco
    <option value="Mozambique">Mozambique
    <option value="Myanmar">Myanmar
    <option value="Namibia">Namibia
    <option value="Nauru">Nauru
    <option value="Nepal">Nepal
    <option value="Netherlands Antilles">Netherlands Antilles
    <option value="Netherlands, The">Netherlands, The
    <option value="New Caledonia">New Caledonia
    <option value="New Zealand">New Zealand
    <option value="Nicaragua">Nicaragua
    <option value="Niger">Niger
    <option value="Nigeria">Nigeria
    <option value="Niue">Niue
    <option value="Norfolk Island">Norfolk Island
    <option value="Northern Mariana Islands">Northern Mariana Islands
    <option value="Norway">Norway
    <option value="Oman">Oman
    <option value="Pakistan">Pakistan
    <option value="Palau">Palau
    <option value="Palestine">Palestine
    <option value="Panama">Panama
    <option value="Papua new Guinea">Papua new Guinea
    <option value="Paraguay">Paraguay
    <option value="Peru">Peru
    <option value="Philippines">Philippines
    <option value="Pitcairn Island">Pitcairn Island
    <option value="Poland">Poland
    <option value="Portugal">Portugal
    <option value="Puerto Rico">Puerto Rico
    <option value="Qatar">Qatar
    <option value="Reunion">Reunion
    <option value="Romania">Romania
    <option value="Russia">Russia
    <option value="Rwanda">Rwanda
    <option value="Saint Helena">Saint Helena
    <option value="Saint Kitts And Nevis">Saint Kitts And Nevis
    <option value="Saint Lucia">Saint Lucia
    <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon
    <option value="Saint Vincent And The Grenadines">Saint Vincent And The Grenadines
    <option value="Samoa">Samoa
    <option value="San Marino">San Marino
    <option value="Sao Tome and Principe">Sao Tome and Principe
    <option value="Saudi Arabia">Saudi Arabia
    <option value="Senegal">Senegal
    <option value="Seychelles">Seychelles
    <option value="Sierra Leone">Sierra Leone
    <option value="Singapore">Singapore
    <option value="Slovakia">Slovakia
    <option value="Slovenia">Slovenia
    <option value="Solomon Islands">Solomon Islands
    <option value="Somalia">Somalia
    <option value="South Africa">South Africa
    <option value="South Georgia And The South Sandwich Islands">South Georgia And The South Sandwich Islands
    <option value="Spain">Spain
    <option value="Sri Lanka">Sri Lanka
    <option value="Sudan">Sudan
    <option value="Suriname">Suriname
    <option value="Svalbard And Jan Mayen Islands">Svalbard And Jan Mayen Islands
    <option value="Swaziland">Swaziland
    <option value="Sweden">Sweden
    <option value="Switzerland">Switzerland
    <option value="Syria">Syria
    <option value="Taiwan">Taiwan
    <option value="Tajikistan">Tajikistan
    <option value="Tanzania">Tanzania
    <option value="Thailand">Thailand
    <option value="Togo">Togo
    <option value="Tokelau">Tokelau
    <option value="Tonga">Tonga
    <option value="Trinidad And Tobago">Trinidad And Tobago
    <option value="Tunisia"Tunisia
    <option value="Turkey">Turkey
    <option value="Turkmenistan">Turkmenistan
    <option value="Turks And Caicos Islands">Turks And Caicos Islands
    <option value="Tuvalu">Tuvalu
    <option value="Uganda">Uganda
    <option value="Ukraine">Ukraine
    <option value="United Arab Emirates">United Arab Emirates
    <option value="United Kingdom">United Kingdom
    <option value="United States of America">United States of America
    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands
    <option value="Uruguay">Uruguay
    <option value="Uzbekistan">Uzbekistan
    <option value="Vanuatu">Vanuatu
    <option value="Vatican City State (Holy See)">Vatican City State (Holy See)
    <option value="Venezuela">Venezuela
    <option value="Vietnam">Vietnam
    <option value="Virgin Islands (British)">Virgin Islands (British)
    <option value="Virgin Islands (US)">Virgin Islands (US)
    <option value="Wallis And Futuna Islands">Wallis And Futuna Islands
    <option value="Yemen">Yemen
    <option value="Yugoslavia">Yugoslavia
    <option value="Zambia">Zambia
    <option value="Zimbabwe">Zimbabwe
    <?php
}
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//This function displays a form for users registeration
function register_user() {
    global $PHP_SELF;
    global $usr;

    //search for cookies to ensure that the user isn't logged in(he shouldn't, because he want to register;)
    //this is ONLY for security reasons
    if ($usr->userpriv()) {
        //sorry, you can't have two accounts ;)
        exit();
    }

    ?>
    <form action="<?php echo $PHP_SELF; ?>" method="POST">
    <p>

    <input type="hidden" name="do_action" value="do_add_us"><br>

    <label for="username">Your Full REAL Name: </label>
    <input type="text" name="us_name_f" id="username"><br>

    <label for="useremail">Your Email: </label>
    <input type="text" name="us_email_f" id="useremail"><br>

    <label for="userbday">Your Birthday: </label>
    <select name="bmonth" >
    <option value="00" selected>Month
    <option value="01">January
    <option value="02">February
    <option value="03">March
    <option value="04">April
    <option value="05">May
    <option value="06">June
    <option value="07">July
    <option value="08">August
    <option value="09">September
    <option value="10">October
    <option value="11">November
    <option value="12">December
    </select><nobr>
    <select name="bday" >
    <option selected>Day
    <option value="01">1
    <option value="02">2
    <option value="03">3
    <option value="04">4
    <option value="05">5
    <option value="06">6
    <option value="07">7
    <option value="08">8
    <option value="09">9
    <option value="10">10
    <option value="11">11
    <option value="12">12
    <option value="13">13
    <option value="14">14
    <option value="15">15
    <option value="16">16
    <option value="17">17
    <option value="18">18
    <option value="19">19
    <option value="20">20
    <option value="21">21
    <option value="22">22
    <option value="23">23
    <option value="24">24
    <option value="25">25
    <option value="26">26
    <option value="27">27
    <option value="28">28
    <option value="29">29
    <option value="30">30
    <option value="31">31
    </select>
    <input type="text" name="byear" size="4" maxlength="4"><br>

    <label for="userfrom">Your Home: </label>
    <select name="us_from_f" id="userfrom">
    <?php
         show_countries();
    ?>
    </select><br>

    <label for="userat">You lives in: </label>
    <select name="us_at_f" id="userat">
    <?php
         show_countries();
    ?>
    </select><br>

    <label for="usskill">Your Skills: </label>
    <input type="text" name="us_skill_f" id="userskill"><br>

    <label for="usdesc">Some details about you: </label>
    <textarea name="us_desc_f" cols="50" id="userdesc" rows="5"></textarea>

    <br><input type="submit" onclick="some_javascript_function_to_insure_that_all_fields_are_valid()" value="Send"> <input type="reset" value="Reset">
    </p>
    </form>
    <?php
}
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//This function adds the user returned by the previous form
function do_register_user() {
    global $mydb;
    global $PHP_SELF;
    global $usr;

    if ($usr->userpriv()) {
        //sorry, you can't have two accounts ;)
        exit();
    }
    if ($usr->isValid()) {
        //sorry, this email is already registered with us ;)
        //hmm.., a choice to send the password to that email :)
        exit();
    }
    
	//what's the date?
	$today = getdate();
	$month = $today['mon'];
	$mday = $today['mday'];
	$year = $today['year'];
	$today = $year.$month.$mday;
	$today = (int)$today;


    //convert special characters to HTML
    $us_skill_p = htmlspecialchars("$us_skill_p");
    $us_desc_p = htmlspecialchars("$us_desc_p");

    //register the user (unapproved)
    
    
    echo "You are now an UNAPPROVED user, your application will be reviewed and approved by an administrator";
    echo "<br>Here is your details :"
    echo "<table>";
    echo "<tr><td>Registeration Date :</td><td>$today</td></tr>";
    echo "<tr><td>Full Name :</td><td>$usr->us_name</td></tr>";
    echo "<tr><td>Email :</td><td>$usr->us_email</td></tr>";
    echo "<tr><td>Birthday :</td><td>$usr->us_bday</td></tr>";
    echo "<tr><td>Home :</td><td>$usr->us_from</td></tr>";
    echo "<tr><td>You lives in :</td><td>$usr->us_at</td></tr>";
    echo "<tr><td>Your Skills :</td><td>$usr->us_skill</td></tr>";
    echo "<tr><td>Details about you :</td><td>$usr->us_desc</td></tr>";

    //email the administrators so they know that this user is waiting to br approved ;)
    
    }
}
//------------------------------------------------------------------------------




?>
